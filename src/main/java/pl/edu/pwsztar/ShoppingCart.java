package pl.edu.pwsztar;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

public class ShoppingCart implements ShoppingCartOperation {

    List<Product> products;

    public ShoppingCart() {
        products = new ArrayList<>();
    }

    public boolean addProducts(String productName, int price, int quantity) {
        if (price < 0 || quantity <= 0)
            return false;

        Product product = findProduct(productName);

        int newQuantity = getSumProductQuantity() + quantity;
        if (newQuantity > PRODUCTS_LIMIT)
            return false;

        if (product != null) {
            if (product.getPrice() != price)
                return false;

            product.setQuantity(product.getQuantity() + quantity);
        } else {
            products.add(new Product(productName, price, quantity));
        }
        return true;
    }

    public boolean deleteProducts(String productName, int quantity) {
        if (quantity <= 0)
            return false;

        Product product = findProduct(productName);

        if (product != null) {
            if (quantity > product.getQuantity())
                return false;

            int newQuantity = product.getQuantity() - quantity;
            if (newQuantity == 0)
                products.remove(product);
            else
                product.setQuantity(newQuantity);
            return true;
        } else return false;
    }

    public int getQuantityOfProduct(String productName) {
        Product product = findProduct(productName);
        if (product != null) {
            return product.getQuantity();
        } else return 0;
    }

    public int getSumProductsPrices() {
        int sum = 0;
        for (Product product : products) {
            sum += product.getQuantity() * product.getPrice();
        }
        return sum;
    }

    public int getProductPrice(String productName) {
        Product product = findProduct(productName);
        if (product != null) {
            return product.getPrice();
        } else return 0;
    }

    public List<String> getProductsNames() {
        return products.stream().map(Product::getProductName).collect(Collectors.toList());
    }

    private Product findProduct(String productName) {
        for (Product product : products) {
            if (product.getProductName().equals(productName)) {
                return product;
            }
        }
        return null;
    }

    private int getSumProductQuantity() {
        int sum = 0;
        for (Product product : products) {
            sum += product.getQuantity();
        }
        return sum;
    }
}
